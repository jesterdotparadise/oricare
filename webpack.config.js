const path = require('path')
module.exports = {
    mode: 'development',
    entry: {
        main: './ts/main.ts',
    },
    output: {
        // filename: 'bundle.js',
        // path: path.resolve(__dirname, 'dist'),
        chunkFilename: '[name].bundle.js',
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist'
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [{
            test: /\.ts$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }, ]
    },
    watchOptions: {
        ignored: /node_modules/
    },
}