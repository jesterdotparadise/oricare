$(function() {

    	//contact form
	const $form = $("#submitForm");
	const $section = $("#email");
	const $subscribe = $("#subscribeForm");
	const $subBox = $("#subscribeBox");

	function showSection(showTag) {
		$section.slideUp();
		$("#email, #loadingRespond, #successRespond").slideUp(function() {
			$(showTag).slideDown();
		});
	}

	function showSection2(showTag) {
		$subscribe.slideUp();
		$("subscribe,#successRespond2").slideUp(function() {
			$(showTag).slideDown();
		});
	}



	$form.on("submit", function(ev) {
		ev.preventDefault();
		if ($form[0].checkValidity() === false) {
			ev.stopImmediatePropagation();
		} else {
			showSection();

			const data = {};
			const formInput = $section.find("input, textarea");
			formInput.each((i, v) => {
				data[$(v).attr("name")] = $(v).val();
			});

			$.post("/mail", data)
				.done(function() {
					showSection("#successRespond");
				})
				.fail(function(e) {
					showSection("#email");
					alert("error", JSON.stringify(e));

				})
				.always(function() {
					// alert("finished");
				});

		}
		$form.addClass('was-validated');
	});


	$subscribe.on("submit", function(ev) {
		ev.preventDefault();
		if ($subscribe[0].checkValidity() === false) {
			ev.stopImmediatePropagation();
		} else {

			const data = {};
			const formInput2 = $subBox.find("input, textarea");
			formInput2.each((i, v) => {
				data[$(v).attr("name")] = $(v).val();
			});

			$.post("/subscribe", data)
				.done(function() {
					showSection2("#successRespond2");
				})
				.fail(function(e) {
					alert("error", JSON.stringify(e));
				})
				.always(function() {
					// alert("finished");
				});
		}
		$subscribe.addClass('was-validated');
	});

});
