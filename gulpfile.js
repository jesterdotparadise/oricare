const {
    series,
    src,
    dest,
    parallel,
    gulp,
    watch
} = require('gulp');
const spawn = require('child_process').spawn;
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const dom = require('gulp-dom');
const fs = require('fs');
const rename = require("gulp-rename");
const glob = require('glob');
const fileArray = glob.sync('./bs4/*.html');
const fileArray2 = glob.sync('./bs4/*.html');
const purgecss = require('gulp-purgecss');
const postcss = require('gulp-postcss');
let node = null;
let tsc = null;

//Make Controller
function makeController(cb) {
    // Step 1 - get list of filenames
    // Step 2 - create contents for controller.js
    // Step 3 - write contents into controller.js
    // Step 1
    let top = "exports.install = function(){";
    let bottom = "";
    fileArray.map(function(file) {
        file = file.substr("/./bs4".length);
        file = file.replace(".html", "");
        // make this > ROUTE("/home", home);
        // top += "ROUTE(\"/" + file + "\", "+ file +");";
        top += `\n\tROUTE("/${file}", ${file});`;
        bottom += `\nfunction ${file}(){\n\tthis.repository.title = "Dotparadise";\n\tthis.view("${file}");\n}`
    });
    top += "\n}";
    // Step 2
    const content = top + bottom;
    // Step 3
    fs.writeFile('./controllers/controllers.js', content, cb);
}


function clearCss(cb) {
    const fontDisplay = require('postcss-font-display');
    const plugins = [
        fontDisplay({display: 'swap', replace: true }),
        autoprefixer(),
        cssnano()
    ];
    src('./bs4/**/*.css')
        .pipe(purgecss({
            content: ['./bs4/**/*.html']
        }))
        .pipe( postcss(plugins) )
        .pipe(dest('./public/'));
    cb();
}

//GenderalBody
function generalBody(cb) {
    fileArray2.map(function() {
        src('./bs4/*.html').pipe(dom(function(body) {
                this.querySelectorAll('header').forEach(a => a.remove());
                // this.querySelector('footer').remove();
                this.querySelectorAll('body script').forEach(srt => srt.remove());
                return this.querySelector('body').innerHTML;
            }, false))
            .pipe(dest('./views/'));
    })
    cb();
}

// //GenderalFooter
// function generalFooter(cb) {
//     fileArray2.map(function() {
//         src('./bs4/*.html').pipe(dom(function(body) {
//                 return this.querySelector('footer').outerHTML;
//             }, false))
//             .pipe(rename("footer.html"))
//             .pipe(dest('./views/shared/'));

//     })
//     cb();
// }

function gulpNav(cb) {
    src('./bs4/home.html').pipe(dom(function() {
            let navItem = this.querySelector('#topBar');
            for (var i = 0; i < navItem.length; i++) {
                //find innertext inside li than do lowercase
                let navName = navItem[i].text.toLowerCase();
                //push the innertext into the class
                navItem[i].setAttribute('class', `@{helpers.isActive('${navName}')} nav-link`);
                //push the innertext to href
                navItem[i].setAttribute('href', `#${navName}`);
            }
            return this.querySelector("nav").outerHTML;
        }))
        .pipe(rename("header.html"))
        .pipe(dest('./views/shared/'));
    cb();
}

//Watch bs4
function watchBs4() {
    watch('./bs4/*.html', generalBody);
}

//Genderal Head link
function generalLink(cb) {
    src('./bs4/home.html').pipe(dom(function(head) {
            this.querySelectorAll('meta,title').forEach(a => a.remove());
            return this.querySelector('head').innerHTML;
        }, false))
        .pipe(rename("link.html")).pipe(dest('./views/shared/'))
    cb();
}


function generalScript(cb) {
    src('./bs4/home.html').pipe(dom(function(head) {
        this.querySelectorAll('section, div, nav').forEach(a => a.remove());
        return this.querySelector('body').innerHTML;
    }, false)).pipe(rename("script.html")).pipe(dest('./views/shared/'))
    cb();
}

function tscWatch(cb) {
    if (tsc) tsc.kill("SIGINT");
    tsc = spawn("tsc", ["-w"], {
        cwd: "./",
        stdio: "inherit",
        shell: true
    });
    tsc.on("close", function(code) {
        if (code === 8) {
            gulp.log("Error detected, waiting for changes...");
        }
    });
    cb();
}

function runServer(cb) {
    if (node) node.kill("SIGINT");
    node = spawn("node", ["debug.js"], {
        cwd: "./",
        stdio: "inherit"
    });
    node.on("close", function(code) {
        if (code === 8) {
            gulp.log("Error detected, waiting for changes...");
        }
    });
    cb();
}
// function doSass(cb) {
// 	return src('sass/**/*.scss')
// 		.pipe(sass({
// 			outputStyle: 'compressed'
// 		})
// 		.on('error', sass.logError))
// 		.pipe(autoprefixer())
// 		.pipe(dest('public/css'));
// 	cb();
// }
function copyAssets(cb) {
    return src('bs4/assets/**/*').pipe(dest('./public/assets'));
    cb();
}

function watchAssets(cb) {
    watch("bs4/assets/**/*", copyAssets);
    cb();
}

function copyHtml(cb) {
    return src('bs4/*.html').pipe(dest('./views'));
    cb();
}
//stop node.js (stop all server)
process.on("exit", (code) => {
    if (node) {
        node.kill("SIGINT");
    }
    if (tsc) {
        tsc.kill("SIGINT");
    }
});
exports.default = series(copyAssets, generalBody,parallel(runServer, watchBs4, watchAssets));
exports.serve = runServer;
exports.tsc = tscWatch;
exports.clearcss = clearCss;
exports.mcontroller = makeController;
exports.chtml = copyHtml;
exports.nav = gulpNav;
exports.mlink = generalLink;
exports.mbody = generalBody;
// exports.mfooter = generalFooter;
exports.mscript = generalScript;
exports.copyfile = copyAssets;
exports.watch = watchAssets;