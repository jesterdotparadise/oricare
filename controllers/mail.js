exports.install = function() {
    ROUTE('POST /mail/', contactForm);
    ROUTE('POST /subscribe/', subscribeForm);
};

function contactForm() {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = twoD(today.getHours()) + ":" + twoD(today.getMinutes()) + ":" + twoD(today.getSeconds());
    const dateTime = date + ' ' + time;
    const title = "Contact Form " + dateTime;

    const items = ["name", "email", "message"];
    for (item of items) {
        if (!this.body[item]) {
            return this.throw400("Invalid Parameters");
        }
    }

    // This function automatically reads view: email.html
    // MAIL('dotparadisellp@gmail.com', title, '~mail', this.body);

    // this.plain('testing');

    return this.success(true, "Enquiry submitted successfully");
}

function twoD(number) {
    return `${number}`.replace(/^(\d)$/, '0$1');
}

function subscribeForm() {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = twoD(today.getHours()) + ":" + twoD(today.getMinutes()) + ":" + twoD(today.getSeconds());
    const dateTime = date + ' ' + time;
    const title = "New Subscriber " + dateTime;

    const items = [ "email"];
    for (item of items) {
        if (!this.body[item]) {
            return this.throw400("Invalid Parameters");
        }
    }

    // This function automatically reads view: email.html
    // MAIL('enquiries@letscospace.com', title, '~mail2', this.body);

    // this.plain('testing');

    return this.success(true, "Enquiry submitted successfully");
}