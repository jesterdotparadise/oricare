exports.install = function() {
	ROUTE("/cn/", home);
	ROUTE("/cn/haircare", haircare);
	ROUTE("/cn/ingredients", ingredients);
	ROUTE("/cn/story", story);
	ROUTE("/cn/contact", contact);
}

function home() {
	setEnvironment2(this.repository, "home");
	this.view("/cn/home");
}

function haircare() {
	setEnvironment2(this.repository, "haircare");
	this.view("/cn/haircare");
}

function story() {
	setEnvironment2(this.repository, "story");
	this.view("/cn/story");
}

function ingredients() {
	setEnvironment2(this.repository, "ingredients");
	this.view("/cn/ingredients");
}

function contact() {
	setEnvironment2(this.repository, "contact");
	this.view("/cn/contact");
}

function setEnvironment2(repo, curItem) {
	repo.title = "Oricare";
	repo.nav= [
	 {class:curItem === "haircare"?"current":"", url: "/cn/haircare", text: "头发护理"},
	 {class:curItem === "ingredients"?"current":"", url: "/cn/ingredients", text: "草本成分"},
	 {class:curItem === "story"?"current":"", url: "/cn/story", text: "我们的故事"},
	 {class:curItem === "contact"?"current":"", url: "/cn/contact", text: "联络我们"}
	];
	repo.banner = "欢迎来到我们的新网站!您的首次订购将可获得免运费优惠,而且无最低消费要求,别错过咯!";
	repo.contact = "拨电/Whatsapp 012-345-6789";
	repo.language = "zh-Hans";
	repo.home = "/cn/";
	repo.pages = curItem;
	if (curItem === "home"){
		repo.pages = "";
	}

}