exports.install = function() {
	ROUTE("/", home);
	ROUTE("/haircare", haircare);
	ROUTE("/ingredients", ingredients);
	ROUTE("/story", story);
	ROUTE("/contact", contact);
}

function home() {
	setEnvironment(this.repository, "home");
	this.view("/home");
}

function haircare() {
	setEnvironment(this.repository, "haircare");
	this.view("/haircare");
}

function story() {
	setEnvironment(this.repository, "story");
	this.view("/story");
}

function ingredients() {
	setEnvironment(this.repository, "ingredients");
	this.view("/ingredients");
}

function contact() {
	setEnvironment(this.repository, "contact");
	this.view("/contact");
}

function setEnvironment(repo, curItem) {
	repo.title = "Oricare";
	repo.nav= [
	 {class:curItem === "haircare"?"current":"", url: "/haircare", text: "Hair Care"},
	 {class:curItem === "ingredients"?"current":"", url: "/ingredients", text: "Botanical Ingredients"},
	 {class:curItem === "story"?"current":"", url: "/story", text: "Our Story"},
	 {class:curItem === "contact"?"current":"", url: "/contact", text: "Contact Us"}
	];
	repo.banner = "Welcome to our new website - Enjoy FREE DELIVERY for your ﬁrst order, no minimum purchase required!";
	repo.pages = curItem;
	repo.contact = "Call/Whatsapp 012-345-6789";
	repo.language = "en";
	repo.home = "/";
	if (curItem === "home"){
		repo.pages = "";
	}

}